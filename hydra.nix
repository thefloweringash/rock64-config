{ config, pkgs, lib, ... }:

let
  inherit (import ./lib.nix) proxyTo;

  hydraBaseDir = "/var/lib/hydra"; # hardcoded upstream

  uploadProducts = with pkgs; writeScript "upload-build-products" ''
    # Upload build outputs
    set -x
    export PATH=${lib.makeBinPath [ jq awscli ]}
    buildStatus=$(jq .buildStatus $HYDRA_JSON)
    if [ "$buildStatus" -ne 0 ]; then
      echo "Not uploading products of unsuccessful build: buildStatus=$buildStatus"
      exit
    fi

    export AWS_DEFAULT_REGION=ap-northeast-1
    upload_path=$(jq --raw-output '"\(.project)/\(.jobset)/\(.job)/\(.build)"' $HYDRA_JSON)
    jq --raw-output '.products[].path' $HYDRA_JSON | \
      while read product; do
        aws s3 cp $product s3://hydra.cons.org.nz/products/$upload_path/
      done
  '';

  uploadOutputs = with pkgs; writeScript "upload-outputs" ''
    set -x
    export PATH=${lib.makeBinPath [ jq nix ]}
    buildStatus=$(jq .buildStatus $HYDRA_JSON)
    if [ "$buildStatus" -ne 0 ]; then
      echo "Not uploading outputs of unsuccessful build: buildStatus=$buildStatus"
      exit
    fi

    export AWS_DEFAULT_REGION=ap-northeast-1
    jq --raw-output '.outputs[].path' $HYDRA_JSON | \
      while read out; do
        nix copy $out --to "s3://hydra.cons.org.nz/cache/?region=$AWS_DEFAULT_REGION"
      done
  '';

  uploadOutputsToCachix = cacheName: with pkgs; writeScript "upload-outputs-to-cachix" ''
    set -x
    export PATH=${lib.makeBinPath [ jq nix cachix ]}
    buildStatus=$(jq .buildStatus $HYDRA_JSON)
    if [ "$buildStatus" -ne 0 ]; then
      echo "Not uploading outputs of unsuccessful build: buildStatus=$buildStatus"
      exit
    fi

    jq --raw-output '.outputs[].path' $HYDRA_JSON | \
      while read out; do
        echo "$out" | cachix push ${cacheName}
      done
  '';
in
{
  services.hydra = {
    enable = true;
    useSubstitutes = true;
    package = pkgs.hydra-unstable;
    hydraURL = "https://hydra.cons.org.nz/";
    notificationSender = "hydra@barbara.cons.org.nz";

    minimumDiskFree          = 5; # hydra-queue-runner
    minimumDiskFreeEvaluator = 5; # hydra-evaluator

    extraConfig = ''
      binary_cache_secret_key_file = /etc/nix/signing.key
      max_output_size = 4294967296
      nar_buffer_size = 12884901888
      <runcommand>
        job = *:*:sdImage
        command = ${uploadProducts}
      </runcommand>
      <runcommand>
        job = *:*:kernel
        command = ${uploadOutputs}
      </runcommand>
      <runcommand>
        job = kevin-nix:*:wireguard
        command = ${uploadOutputs}
      </runcommand>
      <runcommand>
        job = kevin-nix:*:panfrost
        command = ${uploadOutputs}
      </runcommand>
      <runcommand>
        job = lorne-nix:*:*
        command = ${uploadOutputs}
      </runcommand>
      <runcommand>
        job = thefloweringash-armv7:*:*
        command = ${uploadOutputsToCachix "thefloweringash-armv7"}
      </runcommand>
    '';
  };

  # Jam in config env
  systemd.services.hydra-queue-runner.serviceConfig.EnvironmentFile = "/etc/nix/aws-keys-env";

  nix.buildMachines = [
    # Permit hydra to build locally
    {
      hostName = "localhost";
      system = "aarch64-linux";
      maxJobs = 1;
      supportedFeatures = [ "local" "kvm" ];
    }

    # New fast builder
    {
      hostName = "alex.cons.org.nz";
      sshUser = "barbara";
      system = "aarch64-linux";
      maxJobs = 8;
      supportedFeatures = [ "kvm" "big-parallel" ];
    }

    # New fast builder (32-bit vm)
    {
      hostName = "alex7.cons.org.nz";
      sshUser = "barbara";
      system = "armv7l-linux";
      maxJobs = 4;
      supportedFeatures = [ "big-parallel" ];
    }
  ];

  programs.ssh.knownHosts = {
   "alex.cons.org.nz" = { publicKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEls5OHoiPJFuBg7SAiYQcgOoMeQBUCf0Rep7AGqUd18"; };
   "[alex.cons.org.nz]:2200" = { publicKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKgcOpHhIn/NROjAGVIN9wHzlD29Aj/cdATr9Qdszo+P"; };
  };

  # It's broken upstream, and not desired (we use an exporter, not statsd).
  systemd.services.hydra-send-stats.wantedBy = lib.mkForce [];

  programs.ssh.extraConfig = ''
    Host alex7.cons.org.nz
        HostName alex.cons.org.nz
        Port 2200
  '';

  services.nginx = {
    virtualHosts."hydra.cons.org.nz" = {
      enableACME = true;
      forceSSL = true;

      locations."/" = proxyTo "http://127.0.0.1:3000";
    };
  };

  services.postgresqlBackup.enable = true;
  services.postgresqlBackup.databases = [ "hydra" ];
  services.postgresql.package = pkgs.postgresql_14;
  # let postgres log in for backups
  services.postgresql.identMap = ''
    hydra-users postgres postgres
  '';
}
