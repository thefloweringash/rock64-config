{ config, lib, pkgs, ... }:

# The remaining useful parts of rock64-nix, but sending everything to
# serial console.

{
  boot.loader.grub.enable = false;
  boot.loader.generic-extlinux-compatible.enable = true;

  boot.kernelParams = [
    "earlycon=uart8250,mmio32,0xff130000"
    "coherent_pool=1M"
    "ethaddr=\${ethaddr}"
    "eth1addr=\${eth1addr}"
    "serial=\${serial#}"

    # The last console gets the systemd status messages.
    "console=uart8250,mmio32,0xff130000"
  ];
}
