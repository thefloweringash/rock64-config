# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, lib, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./wait-devices.nix
      ./hydra.nix
      ./lorne-nix
      ./thefloweringash-armv7.cachix.org.nix
      ./rock64-configuration.nix

      ## Also not useful, barbara went headless
      #./nixos-desktop
    ];

  nixpkgs.overlays = [
   (import ./overlays/aarch64-fixes.nix)
  ];

  boot.kernelPackages = pkgs.linuxPackages_latest;

  boot.kernel.sysctl."kernel.sysrq" = 1;

  boot.consoleLogLevel = 8;

  boot.tmp.cleanOnBoot = true;

  networking.hostName = "barbara";
  networking.domain = "cons.org.nz";

  nix.settings = {
    max-jobs      = 2; # so we don't block head of line
    cores         = 4; # so single jobs can use the entire system

    sandbox       = true;

    trusted-users = [ "lorne" "bonbori" "illumination" ];
    trusted-public-keys = [
      "barbara.cons.org.nz-1:HplcOsCv3cNAZhCcqT+y9JfhN7DK5lvjc2CHZ1ajfUs="
    ];
    secret-key-files = "/etc/nix/signing.key";
  };

  swapDevices = [ { device = "/var/swapfile"; size = 8192; } ];
  # boot.kernelParams = [ "zswap.enabled=1" ]; # not supported in kernel ?

  # Select internationalisation properties.
  # i18n = {
  #   consoleFont = "Lat2-Terminus16";
  #   consoleKeyMap = "us";
  #   defaultLocale = "en_US.UTF-8";
  # };

  # Set your time zone.
  time.timeZone = "Asia/Tokyo";

  # List packages installed in system profile. To search by name, run:
  # $ nix-env -qaP | grep wget
  environment.systemPackages = with pkgs; [
    silver-searcher
    binutils
    diffstat
    ed
    file
    fzf
    gdb
    git
    gitAndTools.delta
    iftop
    iperf
    kakoune
    multipath-tools
    nix-prefetch-scripts
    openssl
    pv
    python3
    powerline-go
    ruby
    stow
    sysstat
    tcpdump
    tig
    tio
    tmux
    tree
    usbutils
    vim
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.bash.enableCompletion = true;
  # programs.mtr.enable = true;
  # programs.gnupg.agent = { enable = true; enableSSHSupport = true; };
  programs.mosh.enable = true;

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;
  services.openssh.settings = {
    X11Forwarding = true;
    PasswordAuthentication = false;
    KbdInteractiveAuthentication = false;
  };

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [
    80 443 # nginx
  ];
  networking.firewall.rejectPackets = true;
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  system.autoUpgrade.enable = true;
  # It hasn't broken in a while, so let's allow this.
  system.autoUpgrade.allowReboot = true;

  nix.gc.automatic = true;
  nix.gc.dates = "03:00";
  nix.gc.options = "--delete-older-than 7d";

  boot.kernel.sysctl = {
    "kernel.hung_task_panic" = false;
    "kernel.softlockup_panic" = false;
  };

  services.xserver.videoDrivers = [ "modesetting" "fbdev" ];

  security.acme.defaults.email = "lorne@${config.networking.domain}";
  security.acme.acceptTerms = true;

  services.nginx =

  let
    inherit (import ./lib.nix) proxyTo;
  in
  {
    enable = true;

    appendHttpConfig = ''
      server_names_hash_bucket_size 64;
    '';

    virtualHosts."barbara.cons.org.nz" = {
      default = true;

      enableACME = true;
      forceSSL = true;

      locations."/" = {
        extraConfig = ''
          return 307 https://hydra.cons.org.nz$request_uri;
        '';
      };
    };

    virtualHosts."hydra.barbara.cons.org.nz" = {
      enableACME = true;
      forceSSL = true;

      locations."/" = {
        extraConfig = ''
          return 307 https://hydra.cons.org.nz$request_uri;
        '';
      };
    };
  };

  # Enable the KDE Desktop Environment.
  # services.xserver.displayManager.sddm.enable = true;
  # services.xserver.desktopManager.plasma5.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.

  programs.zsh.enable = true;

  users.extraUsers.lorne = {
    isNormalUser = true;
    uid = 1000;
    extraGroups = [ "wheel" "video" "dialout" "docker" ];
    shell = pkgs.zsh;
  };

  users.users.bonbori = {
    isNormalUser = true;
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHPYxcBuQ/DGH2lJd4EQqqq4oeGj/cCeTwJkKhCn7yLz root@bonbori"
    ];
  };

  users.users.illumination = {
    isNormalUser = true;
    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDWHMsLgnIfpX6LLdITDxrnH5puKZZqn8JGcy0K3Y207 root@illumination.cons.org.nz"
    ];
  };

  lorne-nix.emacs.enable = true;
  lorne-nix.monitoring.enable = true;
  services.prometheus.exporters.node. openFirewall = true;

  systemd.coredump.enable = true;
  systemd.extraConfig = ''
    DefaultLimitCORE=infinity
  '';

  virtualisation.containers.enable = true;
  virtualisation.docker.enable = true;

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "23.05"; # Did you read the comment?

}
