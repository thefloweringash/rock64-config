{
  proxyTo = (addr: {
    proxyPass = "${addr}";
    extraConfig = ''
      ## Convey the original intent, for relative urls
      proxy_set_header Host               $host;

      ## Maybe used by someone
      proxy_set_header X-Real-IP          $remote_addr;

      ## Standard forwarding headers
      proxy_set_header X-Forwarded-For    $proxy_add_x_forwarded_for;
      proxy_set_header X-Forwarded-Host   $host;
      proxy_set_header X-Forwarded-Proto  $scheme;

      ## Trust that nginx proxies the remaining headers appropriately
    '';
  });
}
