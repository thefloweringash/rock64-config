{ ... }:
{
  # Copied and pasted from nixpkgs with try=20 replace with try=180 My USB3
  # drive takes 36 seconds to appear after boot. try=20 gives up after 30
  # seconds.
  boot.initrd.postDeviceCommands = ''
    # Function for waiting a device to appear.
    waitDevice() {
        local device="$1"

        # USB storage devices tend to appear with some delay.  It would be
        # great if we had a way to synchronously wait for them, but
        # alas...  So just wait for a few seconds for the device to
        # appear.
        if test ! -e $device; then
            try=180
            echo -n "waiting up to $try times for device $device to appear..."
            while [ $try -gt 0 ]; do
                sleep 1
                # also re-try lvm activation now that new block devices might have appeared
                lvm vgchange -ay
                # and tell udev to create nodes for the new LVs
                udevadm trigger --action=add
                if test -e $device; then break; fi
                echo -n "."
                try=$((try - 1))
            done
            echo
            [ $try -ne 0 ]
        fi
    }
  '';
}
