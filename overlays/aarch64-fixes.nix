self: super: {
  udiskie = (super.udiskie.overrideAttrs (attrs: {
    # docs require asciidoc-full, which requires fop, which requires java,
    # which doesn't build on aarch64
    #
    # TODO: make docs optional in a principled fashion upstream
    postBuild = null;
    postInstall = null;
  })).override { asciidoc-full = null; };
}
